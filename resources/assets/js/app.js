/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require("./bootstrap");
require("izitoast/dist/css/iziToast.css");

window.Vue = require("vue");
window.izitoast = require("izitoast");

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component("listcar-component", require("./components/list-car.vue"));

const app = new Vue({
    el: "#root",
    data: {
        maker: "",
        model: "",
        type: "",
        price: "",
        year: ""
    },
    methods: {
        submitHandler: function(e) {
            e.preventDefault();
            const self = this;
            try {
                this.validate();
                axios
                    .put("seller/car/add", {
                        type: this.type,
                        maker: this.maker,
                        model: this.model,
                        price: this.price,
                        year: this.year
                    })
                    .then(response => {
                        if (response.data.success === true) {
                            self.toastSuccess(response.data.message);
                            self.clear();
                        } else {
                            self.toastError(response.data.message);
                        }
                    })
                    .catch(error => {
                        self.toastError(error.message);
                    });
            } catch (error) {
                self.toastError(error);
            }
        },
        toastSuccess(message) {
            izitoast.success({
                title: "Success!",
                message: message,
                position: "topRight"
            });
        },
        toastError(message) {
            izitoast.error({
                title: "Error!",
                message: message,
                position: "topRight"
            });
        },
        clear() {
            this.maker = "";
            this.model = "";
            this.type = "";
            this.price = "";
            this.year = "";
        },
        validate() {
            if (
                this.maker == "" ||
                this.model == "" ||
                this.type == "" ||
                this.price == "" ||
                this.year == ""
            ) {
                throw "Please complete all fields";
            }
        }
    },
    created() {}
});
