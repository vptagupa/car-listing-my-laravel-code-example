import izitoast from "izitoast";
import "izitoast/dist/css/iziToast.css";

const success = message => {
    izitoast.success({
        title: "Success!",
        message: message,
        position: "topRight"
    });
};

const error = message => {
    izitoast.error({
        title: "Error!",
        message: message,
        position: "topRight"
    });
};

const info = message => {
    izitoast.info({
        title: "Info!",
        message: message,
        position: "topRight"
    });
};

export { success, error, info };
