<form action="seller/add" method="POST" class="text-left" id="form" @submit="submitHandler">
     @csrf
    <div class="form-group">
        <label class="">Type *</label>
        <select class="form-control" name="type" v-model="type">
            <option value="" selected>--SELECT TYPE--</option>
            <option>Hatchback</option>
            <option>Sedan</option>
            <option>MPV</option>
            <option>SUV</option>
            <option>Crossover</option>
            <option>Coupe</option>
            <option>Convertible</option>
        </select>
    </div>
    <div class="form-group">
        <label>Maker *</label>
        <input type="text" class="form-control" name="maker" v-model="maker">
    </div>
    <div class="form-group">
        <label>Model *</label>
        <input type="text" class="form-control" name="model" v-model="model">
    </div>
    <div class="form-group">
        <label>Year *</label>
        <input type="text" class="form-control" name="year" v-model.number="year">
    </div>
    <div class="form-group">
        <label>Price *</label>
        <input type="text" class="form-control" name="price" v-model.number="price">
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
