<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>@yield('title')</title>
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <link href="{{asset('css/app.css')}}" rel="stylesheet" type="text/css">
        <script src="{{ asset('js/app.js') }}" defer></script>
        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }


            .top-right {
                position: absolute;
                right: 10px;
                top: 1%;
            }

            .content {
                text-align: center;
                margin-top: 5%;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }

            .text-right {
                text-align: right !important;
            }

            .text-left {
                text-align: left !important;
            }

            .text-center {
                text-align: center !important;
            }
        </style>

        @yield('header')
    </head>
    <body>
        <div id="root">
            <div class="flex-center position-ref full-height">

                <div class="top-right links">
                    <a href="{{url('/')}}">Home</a>
                    <a href="{{url('seller')}}">Seller</a>
                    <a href="{{ url('car/listing') }}">View Listings</a>
                </div>

                <div class="content">
                    @yield('content')
                </div>
            </div>
        </div>
    </body>
</html>
