@extends('app')

@section('title','Car Listing')
@section('header')

@endsection
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="alert alert-primary" role="alert">
          These car images are just random images. It doesn't base on their types.
        </div>
    </div>
</div>
<listcar-component apibaseurl="{{url("/")}}"></listCar-component>
@endsection
