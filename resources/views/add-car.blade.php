@extends('app')

@section('title','Seller | Car')

<style>
#seller[data-v-sellerform] {
    width: 50%;
    margin: 5% auto;
}
</style>

@section('header')

<!-- <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script> -->
@endsection
@section('content')
<div id="seller" data-v-sellerform>
@include('car-form')
</div>
@endsection
