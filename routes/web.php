<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/seller', 'Seller\Seller@index');

Route::group(['prefix' => 'car'], function($route) {
    $route->get('/listing', 'Seller\Listing@index');
    $route->post('/listing', 'Seller\ListCar@handle');
});

Route::group(['prefix' => 'seller'], function($route) {
    $route->put('/car/add', 'Seller\AddCar@handle');
    $route->patch('/car/update/{id}', 'Seller\UpdateCar@handle');
    $route->delete('/car/delete/{id}', 'Seller\DeleteCar@handle');
    $route->get('/car/get/{id}', 'Seller\GetCar@handle');
});
