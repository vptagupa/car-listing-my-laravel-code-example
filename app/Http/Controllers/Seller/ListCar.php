<?php

namespace App\Http\Controllers\Seller;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Core\Car\CarFactory;
use App\Core\Contracts\Car;
use App\Core\Car\CarService;

class ListCar extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | List Car Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the listing of the car,
    | and return the list of the car in array
    |
    */

    /**
     * Create a new controller instance.
     * @param App\Core\Car\CarService $service
     *
     * @return void
     */
    public function __construct(CarService $service)
    {
        $this->service = $service;
    }

    public function handle(Request $request)
    {
        $list = $this->service->listing(
            $request->get('criteria'),
            $request->get('price_criteria'),
            $request->get('page'),
            $request->get('limit')
        );
        return array(
            'rows' => $list->getTotalRecords(),
            'data' => array_map( function(Car $car) {
               return array(
                   'id' => $car->id()->get(),
                   'type' => $car->getType(),
                   'maker' => $car->getMaker(),
                   'model' => $car->getModel(),
                   'year' => $car->year()->get(),
                   'price' => $car->price()->getWithCurrency(),
                   'image' => $this->randomImage()
               );
           }, $list->get())
        );
    }

    private function randomImage()
    {
        $cars = array(
            asset("images/car1.jpg"),
            asset("images/car2.jpg"),
            asset("images/car3.jpg"),
            asset("images/car4.jpg"),
        );

        return $cars[rand(0, count($cars)-1)];
    }
}
