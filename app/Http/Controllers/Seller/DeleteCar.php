<?php

namespace App\Http\Controllers\Seller;

use App\Http\Controllers\Controller;
use App\Core\Car\CarService;
use App\Core\Car\Id;

class DeleteCar extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Delete Car Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the deletion of the car, it can return success
    | or error whatever the outcome of the service
    |
    */

    /**
     * Create a new controller instance.
     * @param App\Core\Car\CarService $service
     *
     * @return void
     */
     public function __construct(CarService $service)
     {
         $this->service = $service;
     }

    public function handle(int $id)
    {
        try
        {
            $this->service->deleteCar(
                new Id($id)
            );

            return response(array(
                'success' => true,
                'message' => 'Successfuly Deleted!'
            ));
        } catch (\Exception $e) {
            return response(array(
                'success' => false,
                'message' => $e->getMessage()
            ));
        }
    }
}
