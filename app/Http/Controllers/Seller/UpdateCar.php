<?php

namespace App\Http\Controllers\Seller;

use App\Http\Controllers\Controller;
use App\Core\Car\CarService;
use App\Core\Car\Price;
use App\Core\Car\Year;
use App\Core\Car\Id;

use Illuminate\Http\Request;

class UpdateCar extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Update Car Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the update of the car, it can return success
    | or error whatever the outcome of the service
    |
    */

    /**
     * Create a new controller instance.
     * @param App\Core\Car\CarService $service
     *
     * @return void
     */
     public function __construct(Request $request, CarService $service)
     {
         $this->service = $service;
         $this->request = $request;
     }

    public function handle(int $id)
    {
        try
        {
            $this->service->updateCar(
                new Id($id),
                $this->request->get('type'),
                $this->request->get('maker'),
                $this->request->get('model'),
                new Year($this->request->get('year')),
                new Price($this->request->get('price'))
            );

            return response(array(
                'success' => true,
                'message' => 'Successfuly Saved!'
            ));
        } catch (\Exception $e) {
            return response(array(
                'success' => false,
                'message' => $e->getMessage()
            ));
        }
    }
}
