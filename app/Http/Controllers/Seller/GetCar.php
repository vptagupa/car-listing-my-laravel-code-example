<?php

namespace App\Http\Controllers\Seller;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Core\Car\CarFactory;
use App\Core\Car\Id;
use App\Core\Contracts\Car;
use App\Core\Car\CarService;

class GetCar extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Get Car Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the getting and information of the car
    | and return the information into an array
    |
    */

    /**
     * Create a new controller instance.
     * @param App\Core\Car\CarService $service
     *
     * @return void
     */
    public function __construct(CarService $service)
    {
        $this->service = $service;
    }

    public function handle(int $id)
    {
        $car = $this->service->getCar(new Id($id));
        return array(
               'id' => $car->id()->get(),
               'type' => $car->getType(),
               'maker' => $car->getMaker(),
               'model' => $car->getModel(),
               'year' => $car->year()->get(),
               'price' => $car->price()->get(),
               'image' => $this->randomImage()
        );
    }

    private function randomImage()
    {
        $cars = array(
            asset("images/car1.jpg"),
            asset("images/car2.jpg"),
            asset("images/car3.jpg"),
            asset("images/car4.jpg"),
        );

        return $cars[rand(0, count($cars)-1)];
    }
}
