<?php

namespace App\Http\Controllers\Seller;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class Seller extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Seller Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the seller view of adding a car
    |
    */


    /**
     * Create a new controller instance.
     * @param App\Core\Car\CarService $service
     *
     * @return void
     */
    public function __construct()
    {

    }

    public function index()
    {
        return view('add-car');
    }
}
