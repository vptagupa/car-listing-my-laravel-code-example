<?php

namespace App\Http\Controllers\Seller;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class Listing extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Listing Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the view listing of the car
    |
    */


    /**
     * Create a new controller instance.
     * @param App\Core\Car\CarService $service
     *
     * @return void
     */
    public function __construct()
    {

    }

    public function index()
    {
        return view('list-car');
    }
}
