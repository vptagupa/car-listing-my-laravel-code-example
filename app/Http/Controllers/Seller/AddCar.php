<?php

namespace App\Http\Controllers\Seller;

use App\Http\Controllers\Controller;
use App\Core\Car\CarService;
use App\Core\Car\Price;
use App\Core\Car\Year;

use Illuminate\Http\Request;

class AddCar extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Add Car Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the adding of the car, it can return success
    | or error whatever the outcome of the service
    |
    */

    /**
     * Create a new controller instance.
     *
     * @param App\Core\Car\CarService $service
     *
     * @return void
     */
     public function __construct(CarService $service)
     {
         $this->service = $service;
     }

    public function handle(Request $request)
    {
        try
        {
            $car = $this->service->addNewCar(
                $request->get('type'),
                $request->get('maker'),
                $request->get('model'),
                new Year($request->get('year')),
                new Price($request->get('price'))
            );

            return response(array(
                'success' => true,
                'message' => 'Successfuly Saved!'
            ));
        } catch (\Exception $e) {
            return response(array(
                'success' => false,
                'message' => $e->getMessage()
            ));
        }
    }
}
