<?php

namespace App\Core\Car;

use App\Core\Contracts\Validation as ValidationContract;

/**
* This class is for the common validation
*
*/
class Validation implements ValidationContract
{
    /**
    * Validate empty value
    *
    * @param mixed $value
    * @param string $message
    * @throws exception
    * @return void
    */
    public static function notNull($value = "", string $message): void
    {
        if (empty($value) || is_null($value)) {
          throw new \Exception($message, 1);
        }
    }

    /**
    * Custom vallidation
    *
    * @param callable $callback
    * @param string $message
    * @throws exception
    * @return void
    */
    public static function custom(callable $callback, string $message): void
    {
        if(! call_user_func($callback)) {
        throw new \Exception($message, 1);
        }
    }

}
