<?php

namespace App\Core\Car;

use App\Core\Contracts\Id as IdInterface;

/**
* This class is the value object of the car id
*/
class Id implements IdInterface
 {

     /**
     * var int
     */
     private $id;

     public function __construct(int $id)
     {
         if (empty($id) || $id == 0) {
             throw new \Exception("Car Id is required.");
         }
         $this->id = $id;
     }

     /**
     * Get Id
     * @return int
     */
     public function get(): int
     {
         return $this->id;
     }

     /**
     * Get Id
     * @return string
     */
     public function __toString(): string
     {
         return $this->id;
     }
 }
