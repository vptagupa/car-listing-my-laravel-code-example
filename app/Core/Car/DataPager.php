<?php

 namespace App\Core\Car;

use App\Core\Contracts\DataPager as DataPagerInterface;

/**
* This class is for the pagination purposes
*/
class DataPager implements DataPagerInterface
{
     /**
     *
     * var int
     */
     private $total_records = 0;

     /**
     *
     * var array
     */
     private $data = array();

     /**
     *
     * Initialize
     * @param int $totalRecords
     * @param array $data
     */
     public function __construct(int $totalRecords, array $data)
     {
         $this->total_records = $totalRecords;
         $this->data = $data;
     }

     /**
     *
     * Get Total Records
     * @return int
     */
     public function getTotalRecords(): int
     {
         return $this->total_records;
     }

     /**
     *
     * Get Records
     * @return int
     */
     public function get(): array
     {
         return $this->data;
     }
 }
