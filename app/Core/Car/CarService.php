<?php

namespace App\Core\Car;

use App\Core\Car\CarFactory;
use App\Core\Car\Car;
use App\Core\Contracts\Amount;
use App\Core\Contracts\Year;
use App\Core\Contracts\Id;
use App\Core\Contracts\Car as CarInterface;
use App\Core\Contracts\DataPager;

use App\Core\Car\CarRepository;

/**
* This class is a sevices of a car
* It can only execute commands from the factory or repository
*/
class CarService
{
    /**
    * Add new car
    *
    * @param string $type
    * @param string $maker
    * @param string $model
    * @param App\Core\Contracts\Year $year
    * @param App\Core\Contracts\Amount $price
    *
    * @throws exception
    * @return void
    */
    public function addNewCar(
     string $type,
     string $maker,
     string $model,
     Year $year,
     Amount $price)
    {
        $factory = new CarFactory(
            $type,
            $maker,
            $model,
            $year,
            $price
        );

        $car = $factory->makeCar();
        if (empty($car->id()->get())) {
            throw new \Exception("Failed to make a car.");
        }
    }

    /**
    * Update a car
    *
    * @param App\Core\Contracts\Id $id
    * @param string $type
    * @param string $maker
    * @param string $model
    * @param App\Core\Contracts\Year $year
    * @param App\Core\Contracts\Amount $price
    *
    * @throws exception
    * @return void
    */
    public function updateCar(
     Id $id,
     string $type,
     string $maker,
     string $model,
     Year $year,
     Amount $price)
    {
        $repo = new CarRepository;
        $car = $repo->findCar($id);
        if (! $car instanceOf CarInterface) {
            throw new \Exception("This car {$id->get()} could not be found!");
        }

        if (!class_exists( "\App\Core\Car\Types\\".ucFirst(strtolower($type)))) {
            throw new \Exception("Car type did not exists.");
        }

        $car->changeType($type);
        $car->changeMaker($maker);
        $car->changeModel($model);
        $car->changeYear($year);
        $car->changePrice($price);

        $car = $repo->updateCar($car);
        if (!$car) {
            throw new \Exception("Failed to update this car.");
        }
    }

    /**
    * Delete a car
    *
    * @param App\Core\Contracts\Id $id
    *
    * @throws exception
    * @return void
    */
    public function deleteCar(Id $id)
    {
        $repo = new CarRepository;
        $car = $repo->findCar($id);
        if (! $car instanceOf CarInterface) {
            throw new \Exception("This car {$id->get()} could not be found!");
        }

        $car = $repo->deleteCar($car);
        if (!$car) {
            throw new \Exception("Failed to delete this car.");
        }
    }

    /**
    * List a car
    *
    * @return App\Core\Contracts\DataPager
    */
    public function listing(
        array $criteria = array(),
        array $priceCriteria,
        int $page,
        int $limit
    ): DataPager {
        $repo = new CarRepository;
        return $repo->listCar(
            $criteria,
            $priceCriteria,
            $limit,
            ($page - 1) * $limit
        );
    }

    /**
    * Get Car Details
    *
    * @param App\Core\Contracts\Id $id
    * @return App\Core\Contracts\Car
    */
    public function getCar(Id $id): CarInterface
    {
        $repo = new CarRepository;
        return $repo->findCar($id);
    }
}
