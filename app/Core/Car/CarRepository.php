<?php

namespace App\Core\Car;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Core\Contracts\Car as CarInterface;
use App\Core\Contracts\DataPager as DataPagerInterface;
use App\Core\Contracts\Id as IdInterface;
use App\Core\Car\Car;
use App\Core\Car\Id;
use App\Core\Car\Year;
use App\Core\Car\Price;
use App\Core\Car\DataPager;

/**
* This class is a repository of the car
* This is where the crud functions are implemented
* It can only return the car instance instead of database fields
* To make sure that every car output have the same implementation
*/

final class CarRepository extends Model
{
    /**
    * Use soft delete
    * to prevent deletion the records from the databse
    *
    */
    use SoftDeletes;

    /**
    * The table associated with the model.
    *
    * @var string
    */
   protected $table = 'car';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'type', 'maker', 'model','year','price'
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /*
    * List the price attributes
    *
    */
    const CRITERIA_PRICE = array('min_price','max_price');

    /**
    * Save a car
    *
    * @param App\Core\Contracts\Car
    *
    * @return App\Core\Contracts\Car
    */
    public function make(CarInterface $car): CarInterface
    {
        $car =  $this->create(array(
            'type' => $car->getType(),
            'maker' => $car->getMaker(),
            'model' => $car->getModel(),
            'year' => $car->year()->get(),
            'price' => $car->price()->get()
        ));

        return new Car(
            new Id($car->id),
            $car->type,
            $car->maker,
            $car->model,
            new Year($car->year),
            new Price($car->price)
        );
    }

    /**
    * Update a car
    *
    * @param App\Core\Contracts\Car
    *
    * @return bool
    */
    public function updateCar(CarInterface $car): bool
    {
        return $this->where('id', $car->id()->get())->update(array(
            'type' => $car->getType(),
            'maker' => $car->getMaker(),
            'model' => $car->getModel(),
            'year' => $car->year()->get(),
            'price' => $car->price()->get()
        ));
    }

    /**
    * Delete a car
    *
    * @param App\Core\Contracts\Car
    *
    * @return bool
    */
    public function deleteCar(CarInterface $car): bool
    {
        return $this->where('id', $car->id()->get())->delete();
    }

    /**
    * List car
    *
    * @param array $criteria
    * @param array $priceCriteria
    * @param int $limit
    * @param int $offset
    *
    * @return App\Core\Contracts\DataPager
    */
    public function listCar(
        array $criteria = array(),
        array $priceCriteria = array(),
        int $limit = 10,
        int $offset = 0
    ): DataPagerInterface {
        $model = $this;
        if (count($priceCriteria) > 0) {
            $model = $model->where(
                'price',
                '>=',
                $priceCriteria['min_price']
            );
            $model = $model->where(
                'price',
                '<=',
                $priceCriteria['max_price']
            );
        } else {
            foreach($criteria as $key => $value) {
                if (!empty($value) && !empty($key)) {
                    if (!in_array($key, self::CRITERIA_PRICE)) {
                        $model = $model->where($key,'like', '%'.$value.'%');
                    }
                }
            }
        }
        $model_ = $model;
        $model = $model->limit($limit)->offset($offset);
        $cars = array();
        foreach($model->select('*')->get() as $car) {
            $cars[] = new Car(
                new Id($car->id),
                $car->type,
                $car->maker,
                $car->model,
                new Year($car->year),
                new Price($car->price)
            );
        }

        return new DataPager($model_->count(), $cars);
    }

    /**
    * Find a car
    *
    * @param App\Core\Contracts\Id
    *
    * @return App\Core\Contracts\Car
    */
    public function findCar(IdInterface $id): CarInterface {
        $model = $this->find($id->get());

        return new Car(
            new Id($model->id),
            $model->type,
            $model->maker,
            $model->model,
            new Year($model->year),
            new Price($model->price)
        );
    }
}
