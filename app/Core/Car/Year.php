<?php

namespace App\Core\Car;

use App\Core\Contracts\Year as YearInterface;

/**
* This class is the value object of the car year
* It can validate the year
* To control the changing and output of the year
*/
class Year implements  YearInterface
{

     /**
     * var int
     */
     private $year;

     public function __construct(int $year)
     {
         if (strlen($year) < 4 || strlen($year) > 4) {
             throw new \Exception('Year must be a 4 digit.');
         }
         $this->year = $year;
     }

     /**
     * Get Year
     * @return int
     */
     public function get(): int
     {
         return $this->year;
     }

     /**
     * Get Price
     * @return string
     */
     public function __toString(): string
     {
         return $this->year;
     }
 }
