<?php

namespace App\Core\Car;

use App\Core\Contracts\Amount;

/**
* This class is the value object of the car price
* It can validate the price
* To control the changing and output of the price
*/
 class Price implements  Amount
 {
     /**
     * var float
     */
     private $price;

     /**
     * var string
     */
     private $currency;

     public function __construct(float $price, $currency = '$')
     {
         if (empty($price) || $price === 0) {
             throw new \Exception('Price is required.');
         }
         $this->price = $price;
         $this->currency = $currency;
     }

     /**
     * Get Price
     * @return float
     */
     public function get(): float
     {
         return $this->price;
     }

     /**
     * Get Price with Currency
     * @return string
     */
     public function getWithCurrency(): string
     {
         return $this->currency.$this->price;
     }

     /**
     * Get Price
     * @return string
     */
     public function __toString(): string
     {
         return $this->getWithCurrency();
     }

 }
