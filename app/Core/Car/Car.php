<?php

namespace App\Core\Car;

use App\Core\Car\Support\CarTrait;
use App\Core\Contracts\Car as CarInterface;
use App\Core\Contracts\Id;
use App\Core\Contracts\Amount;
use App\Core\Contracts\Year;

/**
* This class is an entity of the car
* and use CarTrait to support the car setters and getters
*/

class Car implements  CarInterface
{
     use CarTrait;

     /**
     * var int
     */
     private $id;

     /**
     * var string
     */
     private $type;

     /**
     * var string
     */
     private $maker;

     /**
     * var string
     */
     private $model;

     /**
     * var Year
     */
     private $year;

      /**
      * var Amount
      */
      private $price;

      /**
      * Initiate car
      *
      * @param App\Core\Contracts\id $id
      * @param string $type
      * @param string $maker
      * @param string $model
      * @param App\Core\Contracts\Year $year
      * @param App\Core\Contracts\Amount $price
      *
      * @return void
      */
      public function __construct(
          ?Id $id,
          string $type,
          string $maker,
          string $model,
          Year $year,
          Amount $price
     ) {
          $this->id = $id;
          $this->type = $type;
          $this->maker = $maker;
          $this->model = $model;
          $this->year = $year;
          $this->price = $price;
      }

     /**
     *
     * Get Car Id
     * @return App\Core\Contracts\Id
     */
     public function id(): ?Id
     {
         return $this->id;
     }

 }
