<?php

namespace App\Core\Car\Support;

use App\Core\Car\CarRepository;
use App\Core\Contracts\Amount;
use App\Core\Contracts\Year;
use App\Core\Car\Validation;

/**
* This class is the setter and getter of the car
*/
trait CarTrait
{
     /**
     *
     * Get Car Maker
     * @return string
     */
     public function getType(): string
     {
         return $this->type;
     }

     /**
     *
     * Get Car Maker
     * @return string
     */
     public function getMaker(): string
     {
         return $this->maker;
     }

     /**
     *
     * Get Car Model
     * @return string
     */
     public function getModel(): string
     {
         return $this->model;
     }

     /**
     *
     * Get Car Year
     * @return Year
     */
     public function year(): Year
     {
         if ($this->year instanceOf Year) {
             return $this->year;
         }
         return new Year($this->year);
     }

     /**
     *
     * Get Car Price
     * @return Amount
     */
     public function price(): Amount
     {
         if ($this->price instanceOf Amount) {
             return $this->price;
         }
         return new Price($this->price);
     }

     /**
     *
     * Change type
     * @param string $type
     *
     * @return void
     */
     public function changeType(string $type): void
     {
         Validation::notNull($type, 'Car Type is required.');
         $this->type = $type;
     }

     /**
     *
     * Change  Maker
     * @param string $maker
     *
     * @return void
     */
     public function changeMaker(string $maker): void
     {
          Validation::notNull($maker, 'Car Maker is required.');
         $this->maker = $maker;
     }

     /**
     *
     * Change Model
     * @param string $model
     *
     * @return void
     */
     public function changeModel(string $model): void
     {
         Validation::notNull($model, 'Car Model is required.');
         $this->model = $model;
     }

     /**
     *
     * Change Year
     * @param App\Core\Contracts\Year $year
     *
     * @return void
     */
     public function changeYear(Year $year): void
     {
         Validation::notNull($year->get(), 'Car Year is required.');
         $this->year = $year;
     }

     /**
     *
     * Change Price
     * @param App\Core\Contracts\Amount $price
     *
     * @return void
     */
     public function changePrice(Amount $price): void
     {
         Validation::notNull($price->get(), 'Car Price is required.');
         $this->price = $price;
     }
 }
