<?php

namespace App\Core\Car\Types;

use App\Core\Contracts\Car;
use App\Core\Contracts\Amount;
use App\Core\Contracts\Year;

/**
* This class is the car type
* Since each car type have different Implementation
* But have common functions
*/
class SUV implements  Car
{
    /**
    * var string
    */
    private $type = 'SUV';

    /**
    * var string
    */
    private $maker;

    /**
    * var string
    */
    private $model;

    /**
    * var Year
    */
    private $year;

    /**
    * var Amount
    */
    private $price;

    /**
    * Initiate car type
    *
    * @param string $maker
    * @param string $model
    * @param App\Core\Contracts\Year $year
    * @param App\Core\Contracts\Amount $price
    *
    * @return void
    */
     public function __construct(
         string $maker,
         string $model,
         Year $year,
         Amount $price
    ) {
         $this->maker = $maker;
         $this->model = $model;
         $this->year = $year;
         $this->price = $price;
     }

     /**
     *
     * Get Car Maker
     * @return string
     */
     public function getType(): string
     {
         return $this->type;
     }
     /**
     *
     * Get Car Maker
     * @return string
     */
     public function getMaker(): string
     {
         return $this->maker;
     }

     /**
     *
     * Get Car Model
     * @return string
     */
     public function getModel(): string
     {
         return $this->model;
     }

     /**
     *
     * Get Car Year
     * @return Year
     */
     public function year(): Year
     {
         return $this->year;
     }

     /**
     *
     * Get Car Price
     * @return Amount
     */
     public function price(): Amount
     {
         return $this->price;
     }

 }
