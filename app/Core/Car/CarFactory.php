<?php

namespace App\Core\Car;

use App\Core\Contracts\Id;
use App\Core\Contracts\Amount;
use App\Core\Contracts\Year;
use App\Core\Contracts\Car as CarInterface;

use App\Core\Car\Car;
use App\Core\Car\CarRepository;
use App\Core\Car\Validation;

/**
* This class is a car factory
* It can only create a car
* and return the instance of a car
* It handles also the validation of the car
*
*/
 class CarFactory
 {
     /**
     * Initiate car
     *
     * @param string $type
     * @param string $maker
     * @param string $model
     * @param App\Core\Contracts\Year $year
     * @param App\Core\Contracts\Amount $price
     *
     * @throws exception
     * @return void
     */
     public function __construct(
         string $type,
         string $maker,
         string $model,
         Year $year,
         Amount $price
    ) {
        $car = "\App\Core\Car\Types\\".ucFirst(strtolower($type));
        if (!class_exists($car)) {
            throw new \Exception("Car type did not exists.");
        }

        Validation::notNull($type, 'Car Type is required.');
        Validation::notNull($maker, 'Car Maker is required.');
        Validation::notNull($model, 'Car Model is required.');
        Validation::notNull($year->get(), 'Car Year is required.');
        Validation::notNull($price->get(), 'Car Price is required.');

         $this->car = new $car(
                $maker,
                $model,
                $year,
                $price
         );
     }

    /**
    * Make a car
    *
    * @return App\Core\Contracts\Car
    */
    public function makeCar(): CarInterface
    {
        $repo = new CarRepository;
        return $repo->make($this->car);
    }
 }
