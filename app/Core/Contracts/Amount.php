<?php

 namespace App\Core\Contracts;


 interface Amount {

     /**
     *
     * Get Price
     * @return float
     */
     public function get(): float;
 }
