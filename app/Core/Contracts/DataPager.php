<?php

 namespace App\Core\Contracts;


 interface DataPager {

     /**
     *
     * Initialize
     * @param int $totalRecords
     * @param array $data
     */
     public function __construct(int $totalRecords, array $data);

     /**
     *
     * Get Total Records
     * @return int
     */
     public function getTotalRecords(): int;

     /**
     *
     * Get Records
     * @return int
     */
     public function get(): array;
 }
