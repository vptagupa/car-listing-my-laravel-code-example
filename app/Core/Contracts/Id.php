<?php

 namespace App\Core\Contracts;


 interface Id {

     /**
     *
     * Get Price
     * @return float
     */
     public function get(): int;
 }
