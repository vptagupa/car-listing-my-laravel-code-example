<?php

namespace App\Core\Contracts;

interface Validation
{
    /**
    * Validate empty value
    *
    * @param mixed $value
    * @param string $message
    * @throws exception
    * @return void
    */
   public static function notNull($value = "", string $message): void;

   /**
   * Custom vallidation
   *
   * @param callable $callback
   * @param string $message
   * @throws exception
   * @return void
   */
   public static function custom(callable $callback, string $message): void;
}
