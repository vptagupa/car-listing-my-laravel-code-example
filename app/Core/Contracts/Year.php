<?php

 namespace App\Core\Contracts;


 interface Year {

     /**
     *
     * Get Year
     * @return int
     */
     public function get(): int;
 }
