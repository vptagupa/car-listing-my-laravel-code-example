<?php

 namespace App\Core\Contracts;

use App\Core\Contracts\Year;
use App\Core\Contracts\Amount;

 interface Car {

     /**
     *
     * Get Car Type
     * @return string
     */
     public function getType(): string;

     /**
     *
     * Get Car Maker
     * @return string
     */
     public function getMaker(): string;

     /**
     *
     * Get Car Model
     * @return string
     */
     public function getModel(): string;

     /**
     *
     * Get Car Year
     * @return Year
     */
     public function year(): Year;

     /**
     *
     * Get Car Price
     * @return Amount
     */
     public function price(): Amount;

 }
