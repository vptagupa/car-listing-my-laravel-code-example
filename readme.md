## About Car Listing

This project is a laravel and vue framework.
It can create, update, delete and search cars

## Project Installation

Execute the commands below

```
composer install
npm install
php artisan migrate
npm run dev
```

## License

The Laravel and Vue framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
